#!/bin/bash

echo "USE syslog_ks;" >> syslog.cql

while read LINE; do

	LINE=${LINE//"'"/}

	echo "INSERT INTO syslog(id, data) VALUES(uuid(), '$LINE');" >> syslog.cql

done < /var/log/syslog_all

cqlsh -f syslog.cql

rm syslog.cql
