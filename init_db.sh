#!/bin/bash

CQL_COMM="

	USE syslog_ks;
	DROP TABLE syslog;
	DROP TABLE syslog_stat;
	DROP KEYSPACE syslog_ks;

	CREATE KEYSPACE syslog_ks WITH replication =
	{
		'class': 'SimpleStrategy',
		'replication_factor': '1'
	};

	USE syslog_ks;

	CREATE TABLE syslog
	(
		id uuid,
		data text,
		PRIMARY KEY( id )
	);

	CREATE TABLE syslog_stat
	(
		timev int,
		times text,
		timet time,
		datet date,
		prior int,
		count int,
		PRIMARY KEY(timev,prior)
	);

	exit;
"
echo "$CQL_COMM" | cqlsh
