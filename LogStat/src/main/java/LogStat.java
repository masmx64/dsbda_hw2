
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;


import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;
import com.datastax.driver.core.LocalDate;

import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;
import scala.Tuple6;


/**
	LogStat - Main programm class
	@author Varykhanov S.S.
	@version 1.0
*/
public class LogStat
{

	/**
	Main method
	@param args - input parametrs 
	*/
	public static void main( String[] args )
	{
		String caddr = ( args.length>0 ? args[0] : "127.0.0.1" );

		SparkConf conf = new SparkConf()
			.setAppName("LogStat")
			.set("spark.cassandra.connection.host",caddr);
			//.set("spark.cassandra.connection.port", "9042");     

		JavaSparkContext sc = new JavaSparkContext( conf );


		//Read raw log data		
		//JavaRDD<String> raw_log = sc.textFile("/home/grey/hw2/syslog");

		/**
		Read raw log data from Cassandra	
		*/
		JavaRDD<String> raw_log = javaFunctions(sc).cassandraTable( "syslog_ks", "syslog", mapColumnTo(String.class) ).select( "data" );

		/**
		Main calc function
		*/
		JavaPairRDD<MsgKey, Integer> reduced = CalcRDD( raw_log );

		/**
		Convert to Tuple	
		*/
		JavaRDD<Tuple6<Integer,String,Long,LocalDate,Integer,Integer>> result =
			reduced.map( a -> new Tuple6<>( 
				a._1().getTime(), a._1().getTimeString(), a._1().getTimeLong(), a._1().getDate(), a._1().getPrior(), a._2() ) );

		/**
		Put data to Cassandra
		*/
		javaFunctions( result )
			.writerBuilder( "syslog_ks", "syslog_stat", mapTupleToRow( Integer.class, String.class, Long.class, LocalDate.class, Integer.class, Integer.class ) )
			.withColumnSelector( someColumns( "timev", "times", "timet", "datet", "prior", "count" ) )
			.saveToCassandra();

		sc.stop();
	}


	/**
	@param - raw_log input RDD of string
	@return - reduced results
	*/
	public static JavaPairRDD<MsgKey, Integer> CalcRDD( JavaRDD<String> raw_log )
	{
		/**
		Parse an convert to (key, count) format
		*/
		JavaPairRDD<MsgKey, Integer> messages = raw_log.mapToPair( a -> new Tuple2<>( new MsgKey(a), 1 ) );

		/**
		Remove invalid rows
		*/
		JavaPairRDD<MsgKey, Integer> messages_fil = messages.filter( a -> a._1().getTime() > 0 );

		/**
		Reduce messages
		*/
		JavaPairRDD<MsgKey, Integer> reduced = messages_fil.reduceByKey( (a,b) -> a+b );

		return reduced;
	}

}














