
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;


import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;
import com.datastax.driver.core.LocalDate;

import scala.Tuple2;
import scala.Tuple3;


/**
	MsgKey class
	@author Varykhanov S.S.
	@version 1.0
*/
public class MsgKey implements Serializable
{
	private final static long interval_sec = 3600L;
	private int time = 0;
	private int prior = 0;

	/**
	Contructor
	*/
	public MsgKey( int t, int p )
	{
		time = t;
		prior = p;
	}

	/**
	Contructor
	@param input log string
	*/
	public MsgKey(String msg)
	{
		String params[] = msg.split("\\|");

		if( params.length < 3 )
		{
			time = 0;
			return;
		}

		try
		{
			prior = Integer.parseInt( params[1] );
		}catch(NumberFormatException e){time=0; return;}

		if( prior < 0 || prior > 7 )
		{
			time = 0;
			return;
		}	

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
		Date date = new Date();
		try
		{
			date = df.parse( params[2] );
		}catch(ParseException e){time=0; return;}

		time = (int)(date.getTime()/1000L/interval_sec);
	}

	/**
	@return return time at ms after 01.01.1970
	*/
	public int getTime()
	{
		return time;
	}

	/**
	@return return time at ms after 01.01.1970 as String format
	*/
	public String getTimeString()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date d = new Date( time * 1000L * interval_sec );
		return df.format( d );
	}

	/**
	@return return time at ms after 01.01.1970 Time only
	*/
	public Long getTimeLong()
	{
		return new Long( ((time*interval_sec) % (24*60*60)) * 1000000000L );
	}

	/**
	@return return time at ms after 01.01.1970 Date only
	*/
	public LocalDate getDate()
	{
		return LocalDate.fromMillisSinceEpoch( time * 1000L * interval_sec );
	}

	/**
	@return return prioriter
	*/
	public int getPrior()
	{
		return prior;
	}

	/**
	Compare two MsgKey
	*/
	public int compareTo(MsgKey a)
	{
		if(time < a.time)
			return -1;
	
		if(time > a.time)
			return 1;

		if(prior < a.prior)
			return -1;
	
		if(prior > a.prior)
			return 1;
	
		return 0;
	}

	/**
	Compare to equals two MsgKey
	*/
	public boolean equals(Object obj)
	{
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MsgKey)) {
			return false;
		}
		MsgKey other = (MsgKey) obj;
		if (time != other.time)
			return false;
		if (prior != other.prior)
			return false;
		return true;
	}

	/**
	Hash code
	*/
	public int hashCode()
	{
		return time * 8 + prior;
	}

	/**
	To string debug
	*/
	@Override
	public String toString()
	{
		return MessageFormat.format( "{0},{1}", time, prior );
	}
}















