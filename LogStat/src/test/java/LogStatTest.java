
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import org.junit.*;
import static org.junit.Assert.fail;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;


import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;
import com.datastax.driver.core.LocalDate;

import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;
import scala.Tuple6;


/**
	Unit tests class
	@author Varykhanov S.S.
	@version 1.0
*/
public class LogStatTest
{

	/**
	Test MsgKey class
	*/
    @Test
    public void testMsgKey()
	{
		MsgKey mk = new MsgKey("3|6|2017-11-30T00:05:41.523085+03:00|grey-vb|systemd[1]:| Starting Network Manager Script Dispatcher Service...");

		if( mk.getTime() != 419997 )
			fail("MsgKey: time");

		if( mk.getPrior() != 6 )
			fail("MsgKey: prior");

		System.out.println( mk.getTimeString() );
		if( ! mk.getTimeString().equals("11/30/2017 00:00:00") )
			fail("MsgKey: time to string");
	}


	/**
	Test MsgKey incorrect 1
	*/
    @Test
    public void testMsgKeyIncorrect1()
	{
		MsgKey mk = new MsgKey("");

		if( mk.getTime() != 0 )
			fail("MsgKey: incorrect");
	}


	/**
	Test MsgKey incorrect 2
	*/
    @Test
    public void testMsgKeyIncorrect2()
	{
		MsgKey mk = new MsgKey("3|6|201ff7-11-30T00:05:41.523085+03:00|grey-vb|systemd[1]:| Starting Network Manager Script Dispatcher Service...");

		if( mk.getTime() != 0 )
			fail("MsgKey: incorrect");
	}


	/**
	Test MsgKey incorrect 3
	*/
    @Test
    public void testMsgKeyIncorrect3()
	{
		MsgKey mk = new MsgKey("3|8|2017-11-30T00:05:41.523085+03:00|grey-vb|systemd[1]:| Starting Network Manager Script Dispatcher Service...");

		if( mk.getTime() != 0 )
			fail("MsgKey: incorrect");
	}


	/**
	Test MsgKey incorrect 4
	*/
    @Test
    public void testMsgKeyIncorrect4()
	{
		MsgKey mk = new MsgKey("3|ss|2017-11-30T00:05:41.523085+03:00|grey-vb|systemd[1]:| Starting Network Manager Script Dispatcher Service...");

		if( mk.getTime() != 0 )
			fail("MsgKey: incorrect");
	}


	/**
	Test normal work
	*/
    @Test
    public void testSpark()
	{
		SparkConf conf = new SparkConf()
			.set("spark.driver.host", "localhost")
			.set("spark.testing.memory", "2147480000");

		JavaSparkContext sc = new JavaSparkContext( "local[1]", "LogStatTest", conf );

		List<String> dataset = Arrays.asList
		(
			new String("3|6|2017-11-30T13:32:12.827091+03:00|grey-vb|systemd[1]:| Time has been changed"),
			new String("1|7|2017-11-29T00:25:51.555500+03:00|grey-vb|root:| Nov 29 00:25:51 127.0.0.1 service: Error Event"),
			new String("1|7|2017-11-29T00:25:51.557682+03:00|grey-vb|root:| Nov 29 00:25:51 127.0.0.1 service: Info Event"),
			new String("3|6|2017-11-30T00:05:41.523085+03:00|grey-vb|systemd[1]:| Starting Network Manager Script Dispatcher Service..."),
			new String("3|6|2017-11-30T00:05:41.528286+03:00|grey-vb|dhclient[24144]:| bound to 10.0.2.15 -- renewal in 39988 seconds."),
			new String("3|5|2017-11-30T00:05:41.530426+03:00|grey-vb|dbus[742]:| [system] Successfully activated service org.freedesktop.nm_dispatcher"),
			new String("3|7|2017-11-30T13:32:13.098095+03:00|grey-vb|ntpd[5235]:| new interface(s) found: waking up resolver"),
			new String("10|5|2017-11-30T13:33:14.459309+03:00|grey-vb|pkexec[25451]:| grey: Executing command [USER=root] [TTY=unknown] [CWD=/home/grey] [COMMAND=/usr/lib/update-notifier/package-system-locked]")

		);

		HashMap<MsgKey, Integer> results = new HashMap<MsgKey, Integer>();
		results.put( new MsgKey( 419997, 5 ), new Integer(1) );
		results.put( new MsgKey( 420010, 6 ), new Integer(1) );
		results.put( new MsgKey( 419997, 6 ), new Integer(2) );
		results.put( new MsgKey( 420010, 5 ), new Integer(1) );
		results.put( new MsgKey( 420010, 7 ), new Integer(1) );
		results.put( new MsgKey( 419973, 7 ), new Integer(2) );


		JavaRDD<String> raw_log = sc.parallelize( dataset );

		JavaPairRDD<MsgKey, Integer> reduced = LogStat.CalcRDD( raw_log );

		List<Tuple2<MsgKey, Integer>> out = reduced.collect();

		if( out.size() != 6 )
			fail("Incorrect results size");

		for(Iterator<Tuple2<MsgKey, Integer>> i = out.iterator(); i.hasNext();)
		{
			Tuple2<MsgKey, Integer> item = i.next();

			if( ! results.containsKey( item._1() ) )
				fail("Cant find MsgKey");

			/*System.out.println( item._1().toString() );
			System.out.println( item._2() );
			System.out.println( results.get( item._1() ) );*/

			if( results.get( item._1() ).intValue() != item._2().intValue() )
				fail("Incorrect count");

		}

		sc.stop();
	}


	/**
	Test normal work
	*/
    @Test
    public void testSparkIncorrect()
	{
		SparkConf conf = new SparkConf()
			.set("spark.driver.host", "localhost")
			.set("spark.testing.memory", "2147480000");

		JavaSparkContext sc = new JavaSparkContext( "local[1]", "LogStatTest", conf );

		List<String> dataset = Arrays.asList
		(
			new String("3|6|2017-11-30T13:32:12.827091+03:00|grey-vb|systemd[1]:| Time has been changed"),
			new String("1|-99|2017-11-29T00:25:51.555500+03:00|grey-vb|root:| Nov 29 00:25:51 127.0.0.1 service: Error Event"),
			new String("1|7|2017-11-29T00:25:51.557682+03:00|grey-vb|root:| Nov 29 00:25:51 127.0.0.1 service: Info Event"),
			new String("FFFFFFFFFUUUUUUUU!!!!!!!!!!"),
			new String("3|6|2017-11-30T00:05:41.528286+03:00|grey-vb|dhclient[24144]:| bound to 10.0.2.15 -- renewal in 39988 seconds."),
			new String("3|5|2017-11-30T00:05:41.530426+03:00|grey-vb|dbus[742]:| [system] Successfully activated service org.freedesktop.nm_dispatcher"),
			new String("3|7|2017-11-30T13:32:13.098095+03:00|grey-vb|ntpd[5235]:| new interface(s) found: waking up resolver"),
			new String("10|5|20XXXXXXX:33:14.459309+03:00|grey-vb|pkexec[25451]:| grey: Executing command [USER=root] [TTY=unknown] [CWD=/home/grey] [COMMAND=/usr/lib/update-notifier/package-system-locked]")

		);

		HashMap<MsgKey, Integer> results = new HashMap<MsgKey, Integer>();
		results.put( new MsgKey( 419997, 5 ), new Integer(1) );
		results.put( new MsgKey( 420010, 6 ), new Integer(1) );
		results.put( new MsgKey( 419997, 6 ), new Integer(1) );
		results.put( new MsgKey( 420010, 7 ), new Integer(1) );
		results.put( new MsgKey( 419973, 7 ), new Integer(1) );


		JavaRDD<String> raw_log = sc.parallelize( dataset );

		JavaPairRDD<MsgKey, Integer> reduced = LogStat.CalcRDD( raw_log );

		List<Tuple2<MsgKey, Integer>> out = reduced.collect();

		if( out.size() != 5 )
			fail("Incorrect results size");

		for(Iterator<Tuple2<MsgKey, Integer>> i = out.iterator(); i.hasNext();)
		{
			Tuple2<MsgKey, Integer> item = i.next();

			if( ! results.containsKey( item._1() ) )
				fail("Cant find MsgKey");

			//System.out.println( item._1().toString() );
			//System.out.println( item._2() );
			//System.out.println( results.get( item._1() ) );

			if( results.get( item._1() ).intValue() != item._2().intValue() )
				fail("Incorrect count");

		}

		sc.stop();
	}

}














